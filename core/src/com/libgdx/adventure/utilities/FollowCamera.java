package com.libgdx.adventure.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * A camera that either follows an object with fixed viewport size or that you
 * can displace and zoom with the mouse or keyboard.
 */

public class FollowCamera {

    private ScreenViewport screenViewport;
    private int screenWidth;
    private int screenHeight;
    boolean renderAlways=false;

    private OrthographicCamera camera;
    private float worldWidth;
    private float worldHeight;

    private boolean following = true;
    private boolean mouseControl=true;
    private boolean keyboardControl=true;

    private int mouseButton = Input.Buttons.LEFT;

    private int toggleFollowingKey = Input.Keys.SPACE;                 //  you can change this
    private int upKey = Input.Keys.NUMPAD_8;                            // "UP" on numpad
    private int downKey = Input.Keys.NUMPAD_2;
    private int leftKey = Input.Keys.NUMPAD_4;
    private int rightKey = Input.Keys.NUMPAD_6;
    private int zoomOutKey = Input.Keys.NUMPAD_9;                      // pg up
    private int zoomInKey = Input.Keys.NUMPAD_3;

    private float minZoom = 0.1f;
    private final float D_ZOOM_MOUSE=1.1f;                       // as a multiplication factor
    private final float D_ZOOM_KEYBOARD=1.02f;                       // as a multiplication factor
    private final float D_POSITION=0.02f;                     // as world fraction

    /**
     * make a follow camera using an existing camera. Sets world dimensions equal to camera viewport
     *
     * @param worldWidth
     * @param worldHeight
     * @param camera
     */
    public FollowCamera(float worldWidth,float worldHeight,OrthographicCamera camera) {
        this.camera = camera;
        setWorldDimensions(worldWidth,worldHeight);
        screenViewport=new ScreenViewport();
    }

    /**
     * update call in resize
     */
    public void update(int width,int height){
        screenWidth=width;
        screenHeight=height;
        screenViewport.update(width, height,true);
    }

    /**
     * use a different camera, just in case
     *
     * @param camera
     * @return this, for chaining, simalarly as in builder pattern
     */
    public FollowCamera setCamera(OrthographicCamera camera) {
        this.camera = camera;
        limit();
        return this;
    }

    /**
     * change the world dimensions, maybe for a new larger level
     *
     * @param width of the world in "world units"
     * @param height of the world
     * @return this
     */
    public FollowCamera setWorldDimensions(float width, float height) {
        worldWidth = width;
        worldHeight = height;
        limit();
        return this;
    }

    public FollowCamera setRenderAlways(boolean renderAlways){
        this.renderAlways=renderAlways;
        return this;
    }

    /**
     * input listener for the mouse wheel
     */
    public class ScrollZoom extends InputAdapter {
        @Override
        public boolean scrolled(int amount) {
            boolean mouseScroll = mouseControl && !following;
            if (mouseScroll) {
                Logger.log("z " + camera.zoom);
                if (amount > 0) {
                    camera.zoom *= D_ZOOM_MOUSE;
                } else {
                    camera.zoom /= D_ZOOM_MOUSE;
                }
            }
            return mouseScroll;
        }
    }

    /**
     * Easiest way to attach a ScrollZoom mouse wheel listener to input if we do not need an inputMultiplexer.
     *
     * @return this for chaining (similar to builder pattern)
     */
    public FollowCamera useMouseWheel() {
        Gdx.input.setInputProcessor(new ScrollZoom());
        return this;
    }

    /**
     * activate or disable mouse control
     *
     * @param onOff
     * @return this for chaining
     */
    public FollowCamera useMouseControl(boolean onOff){
        mouseControl=onOff;
        return this;
    }

    /**
     * activate or disable keyboard control
     *
     * @param onOff
     * @return this for chaining
     */
    public FollowCamera useKeyboardControl(boolean onOff){
        keyboardControl=onOff;
        return this;
    }

    /**
     *   choose a different mouse button. Input.Buttons.LEFT instead of default RIGHT.
     *
     * @param button   Input.Buttons
     * @return this
     */

    public FollowCamera setMouseButton(int button) {
        mouseButton = button;
        return this;
    }

    /**
     * alternative keyboard layout for "qwertz"-keyboards
     *
     * @return this for chaining
     */
    public FollowCamera wasyexKeys(){
        upKey=Input.Keys.W;
        downKey=Input.Keys.Y;
        leftKey=Input.Keys.A;
        rightKey=Input.Keys.S;
        zoomOutKey=Input.Keys.E;
        zoomInKey=Input.Keys.X;
        return this;
    }

    /**
     * change to minimum zoom level, increase if magnification is too large
     *
     * @param minZoom
     * @return this
     */
    public FollowCamera setMinZoom(float minZoom) {
        this.minZoom = minZoom;
        limit();
        return this;
    }

    /**
     * switch from controlled to following(true) or from following to controlled (false)
     *
     * @param following
     * @return this
     */
    public FollowCamera setFollowing(boolean following) {
        this.following = following;
        return this;
    }

    /**
     * check if camera is following or controlled, just in case.
     *
     * @return true if camera is following
     */
    public boolean isFollowing(){
        return following;
    }


    /**
     * Limit zoom and position of camera to show only the given world dimensions. Limit magnification.
     */
    private void limit() {
        // zoom for game dev: can see whole world
        float maxZoom = Math.max(worldWidth / camera.viewportWidth, worldHeight / camera.viewportHeight);
        // to be safe in case that maxZoom<minZoom, then zoom=maxZoom
        camera.zoom=Math.min(maxZoom,Math.max(minZoom,camera.zoom));
        // camera.zoom - effective camera viewport increases with increasing zoom
        // increasing zoom zooms out and shows more of the world
        // now be careful, the effective viewport may be larger than the world,limit to world dimensions
        float effectiveViewportWidth2 = 0.5f * Math.min(camera.viewportWidth * camera.zoom,worldWidth);
        float effectiveViewportHeigth2 = 0.5f * Math.min(camera.viewportHeight * camera.zoom,worldHeight);
        camera.position.x = MathUtils.clamp(camera.position.x, effectiveViewportWidth2,
                worldWidth - effectiveViewportWidth2);
        camera.position.y = MathUtils.clamp(camera.position.y, effectiveViewportHeigth2,
                worldHeight - effectiveViewportHeigth2);
    }

    /**
     * indicate position and size on the screen
     */
    public void renderFrame(ShapeRenderer shapeRenderer){
        screenViewport.apply();
        float rectWidth=camera.zoom*camera.viewportWidth*screenWidth/worldWidth;
        float rectHeight=camera.zoom*camera.viewportHeight*screenHeight/worldHeight;
        float rectCornerX=camera.position.x*screenWidth/worldWidth-0.5f*rectWidth;
        float rectCornerY=camera.position.y*screenHeight/worldHeight-0.5f*rectHeight;

        shapeRenderer.setProjectionMatrix(screenViewport.getCamera().combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.rect(rectCornerX,rectCornerY,rectWidth,rectHeight);

        shapeRenderer.end();
    }

    /**
     * Follow a given position or control with mouse and keyboard. Call in renderFrame().
     *
     * @param v Vector2 position
     */
    public void follow(Vector2 v){
        follow(v.x,v.y);
    }

    /**
     * Follow a given position or control with mouse and keyboard. Call in renderFrame().
     *
     * @param x float x-coordinate to follow
     * @param y float y-coordinate
     */
    public void follow(float x,float y){
        if (Gdx.input.isKeyJustPressed(toggleFollowingKey)){
            following=!following;
        }
        if (following){
            camera.zoom=1f;
            camera.position.set(x,y,0f);
        }
        else {
            if (mouseControl){
                if (Gdx.input.isButtonPressed(mouseButton)){
                    camera.position.set((float)Gdx.input.getX()/Gdx.graphics.getWidth()*worldWidth,
                            (1f-(float)Gdx.input.getY()/Gdx.graphics.getHeight())*worldHeight,0f);
                }
            }
            if (keyboardControl){
                if (Gdx.input.isKeyPressed(zoomInKey)){
                    camera.zoom/=D_ZOOM_KEYBOARD;
                }
                if (Gdx.input.isKeyPressed(zoomOutKey)){
                    camera.zoom*=D_ZOOM_KEYBOARD;
                }
                float dPosition=D_POSITION*0.5f*(worldWidth+worldHeight)*camera.zoom;
                if (Gdx.input.isKeyPressed(downKey)){
                    camera.position.y-=dPosition;
                }
                if (Gdx.input.isKeyPressed(upKey)){
                    camera.position.y+=dPosition;
                }
                if (Gdx.input.isKeyPressed(rightKey)){
                    camera.position.x+=dPosition;
                }
                if (Gdx.input.isKeyPressed(leftKey)){
                    camera.position.x-=dPosition;
                }
            }
        }
        limit();
    }
}
