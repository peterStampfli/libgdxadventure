package com.libgdx.adventure.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.libgdx.adventure.elements.Elemental;

import static com.badlogic.gdx.Gdx.input;

/**
 * Created by peter on 11/26/16.
 */

public class TouchHandler {

    private UserInteraction userInteraction;
    private Elemental element;
    private boolean elementIsSelected;                                 // element is really selected
    // if there was touch in previous call, we need this because we do polling
    private boolean wasTouching;
    //  positions of last touch and new touch
    private Vector2 oldPosition;
    private Vector2 newPosition;
    //  position of touch as average (dragging) and displacement
    private Vector2 position;
    private Vector2 deltaPosition;


    /**
     * create a touch handler
     *
     * @param elemental       object,its elemental methods get called, element or elements
     * @param userInteraction object for reading touch position, depends on camera
     */
    public TouchHandler(Elemental elemental, UserInteraction userInteraction) {
        this.element = elemental;
        elementIsSelected = false;
        wasTouching = false;
        this.userInteraction = userInteraction;
        oldPosition = new Vector2();
        newPosition = new Vector2();
        position = new Vector2();
        deltaPosition = new Vector2();
    }

    private void updatePosition() {
        position.set(oldPosition).add(newPosition).scl(0.5f);
        deltaPosition.set(newPosition).sub(oldPosition);
    }

    private boolean touchBeginSelection() {
        userInteraction.readTouch(newPosition);
        oldPosition.set(newPosition);
        updatePosition();
        return element.isSelected(position);
    }

    private boolean touchBeginAction() {
        return element.touchBegin(position);
    }

    private boolean touchDrag() {
        oldPosition.set(newPosition);
        userInteraction.readTouch(newPosition);
        updatePosition();
        return element.touchDrag(position, deltaPosition);
    }

    private boolean touchEnd() {
        oldPosition.set(newPosition);
        updatePosition();
        return element.touchEnd();
    }


    /**
     * update touch data and call events
     * make sure all position data is well defined, even when not used
     */
    public void update() {
        // get touch event, for mouse only if the left button is pressed
        boolean isTouching = Gdx.input.isTouched() && input.isButtonPressed(Input.Buttons.LEFT);
        boolean somethingHappened = false;

        if (!wasTouching && isTouching) {
            elementIsSelected = touchBeginSelection();
            if (elementIsSelected) {
                somethingHappened = touchBeginAction();
            }
        }

        if (wasTouching && isTouching) {
            if (elementIsSelected) {
                somethingHappened = touchDrag();
            }
        }

        if (wasTouching && !isTouching) {
            if (elementIsSelected) {
                somethingHappened = touchEnd();
            }
            elementIsSelected = false;
        }
        
        wasTouching = isTouching;

        if (somethingHappened) {
            Basic.requestRendering();
        }
    }
}
