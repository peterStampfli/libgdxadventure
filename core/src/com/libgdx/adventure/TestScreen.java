package com.libgdx.adventure;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.libgdx.adventure.elements.DiscRenderer;
import com.libgdx.adventure.elements.DiscSelector;
import com.libgdx.adventure.elements.Element;
import com.libgdx.adventure.elements.ElementNullActions;
import com.libgdx.adventure.elements.SimpleTouchMove;
import com.libgdx.adventure.utilities.Basic;
import com.libgdx.adventure.utilities.Device;
import com.libgdx.adventure.utilities.TouchHandler;
import com.libgdx.adventure.utilities.UserInteraction;

/**
 * Created by peter on 11/29/16.
 */

public class TestScreen extends ScreenAdapter {
    private final LibGdxAdventure game;
    private final Device device;
    private Viewport viewport;
    private UserInteraction userInteraction;
    private ShapeRenderer shapeRenderer;
    private TouchHandler touchHandler;

    private Element element;

    public TestScreen(LibGdxAdventure libGdxAdventure) {
        game = libGdxAdventure;
        device = game.device;
        shapeRenderer = device.shapeRenderer;
        viewport = new ScreenViewport();
        userInteraction = new UserInteraction(viewport.getCamera());
        ElementNullActions elementNullActions = new ElementNullActions();
        element = new Element(new ElementNullActions());
        element.renderer = new DiscRenderer(30, Color.YELLOW, shapeRenderer);
        element.touchDragger = new SimpleTouchMove();
        element.selector = new DiscSelector(30);
        touchHandler = new TouchHandler(element, userInteraction);
        element.position.set(200, 200);

    }

    @Override
    public void show() {
        Basic.setContinuousRendering(true);
    }

    @Override
    public void resize(int width, int height) {
        userInteraction.update(width, height);
        //camera.setToOrtho(false,width,height);
        viewport.update(width, height, true);
    }

    @Override
    public void render(float dTime) {

        touchHandler.update();
        // userInteraction.readTouch(element.position);
        viewport.apply();
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        Basic.clearBackground(Color.CYAN);

        shapeRenderer.setColor(Color.RED);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        shapeRenderer.rect(0, 0, 100, 100);
        element.render();

        shapeRenderer.end();


    }


}
