package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by peter on 11/30/16.
 */

public class ElementNullTouchDragger implements ElementTouchDragger {
    @Override
    public boolean touchDrag(Element element, Vector2 position, Vector2 deltaPosition) {
        return false;
    }
}
