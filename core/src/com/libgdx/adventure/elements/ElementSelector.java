package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by peter on 11/29/16.
 */

public interface ElementSelector {

    boolean isSelected(Element element, Vector2 position);
}
