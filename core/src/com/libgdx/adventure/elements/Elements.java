package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;
import com.libgdx.adventure.utilities.Logger;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by peter on 11/26/16.
 */

public class Elements implements Elemental {

    private boolean reorder=false;
    private String label;
    private LinkedList<Elemental> elements;
    private Elemental element;                               // the selected element
    private boolean elementIsSelected =false;                                 // element is really selected


    public Elements(String label){
        this.label=label;
        elements =new LinkedList<Elemental>();
    }

    public void add(Elemental newElement){
        elements.add(newElement);
    }

    public void remove(Elemental oldElement){
        elements.remove(oldElement);
        if (oldElement== element){
            elementIsSelected =false;
        }
    }

    public void setReorder(boolean reorder){
        this.reorder=reorder;
    }

    @Override
    public void render() {
        Logger.log(label +"starts renderFrame");

        Iterator<Elemental> elementIterator= elements.descendingIterator();
        while (elementIterator.hasNext()){
            elementIterator.next().render();
        }
        Logger.log(label +"ends renderFrame");

    }

    /**
     * search the elements for one selected by touch, call at touch begin, further touch events
     *  go to this element.
     *  if reorder = true, then element becomes first element
     *  if nothing is selected then elementIsSelected=false and element=null
     *
     * @param position of touch
     * @return true if an element contains touch
     */
    @Override
    public boolean isSelected(Vector2 position) {
        Logger.log(label +" starts is selected");
        ListIterator<com.libgdx.adventure.elements.Elemental>elementIterator= elements.listIterator();
        while (elementIterator.hasNext()){
            element =elementIterator.next();
            elementIsSelected =(element.isSelected(position));
            if (elementIsSelected){
                if (reorder) {
                    elementIterator.remove();
                    elements.addFirst(element);
                }
                break;
            }
        }
        Logger.log(label+" is selected= "+ elementIsSelected);
        if (!elementIsSelected){
            element =null;
        }
        return elementIsSelected;
    }

    /**
     * if an element has been selected, call its touchBegin-method
     * @param position of touch
     * @return true if there has something changed and we need redraw
     */
    @Override
    public boolean touchBegin(Vector2 position) {
        Logger.log(label +"starts touchbegin");
        if (elementIsSelected){
            return element.touchBegin(position);
        }
        return false;
    }

    /**
     * if an element has been selected, call its touchDrag-method
     * @param position of touch
     * @param deltaPosition  displacement of touch
     * @return true if there has something changed and we need redraw
     */
    @Override
    public boolean touchDrag(Vector2 position, Vector2 deltaPosition) {
        Logger.log(label +"starts touchDrag");
        if (elementIsSelected){
            return element.touchDrag(position,deltaPosition);
        }
        return false;
    }

    /**
     * if an element has been selected, call its touchEnd-method
     *   set elementIsSelected to false and element=null
     * @return true if there has something changed and we need redraw
     */
    @Override
    public boolean touchEnd() {
        Logger.log(label +"starts touchEnd");
        boolean changed= elementIsSelected;
        if (elementIsSelected){
            changed= element.touchEnd();
        }
        elementIsSelected =false;
        element =null;
        return changed;
    }
}
