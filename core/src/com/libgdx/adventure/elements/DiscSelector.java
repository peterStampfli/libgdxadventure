package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by peter on 11/29/16.
 */

public class DiscSelector implements ElementSelector {

    private float rSquare;

    public DiscSelector(float diameter){
        rSquare=0.25f*diameter*diameter;
    }

    @Override
    public boolean isSelected(Element element, Vector2 position) {
        float dx=element.position.x-position.x;
        float dy=element.position.y-position.y;
        return (rSquare > dx*dx+dy*dy);
    }
}
