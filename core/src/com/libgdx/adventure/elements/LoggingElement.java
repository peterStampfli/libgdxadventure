package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;
import com.libgdx.adventure.utilities.Logger;

/**
 * Created by peter on 11/26/16.
 */

public class LoggingElement implements Elemental {

    public boolean isSelection =false;
    public String label="missing label";

    public LoggingElement(String label){
        this.label=label;
    }


    @Override
    public void render() {
        Logger.log("Element "+label+" renders ");
    }

    @Override
    public boolean isSelected(Vector2 position) {
        Logger.log("Element "+label+" isSelected= "+ isSelection);
        return isSelection;
    }

    @Override
    public boolean touchBegin(Vector2 position) {
        Logger.log("Element "+label+" touch begin");
        return true;
    }

    @Override
    public boolean touchDrag(Vector2 position, Vector2 deltaPosition) {
        Logger.log("Element "+label+" touch drag");
        return false;
    }

    @Override
    public boolean touchEnd() {
        Logger.log("Element "+label+" touch end");
        return false;
    }
}
