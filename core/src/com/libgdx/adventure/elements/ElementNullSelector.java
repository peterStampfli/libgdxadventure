package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by peter on 11/30/16.
 */

public class ElementNullSelector implements ElementSelector {
    @Override
    public boolean isSelected(Element element, Vector2 position) {
        return false;
    }
}
