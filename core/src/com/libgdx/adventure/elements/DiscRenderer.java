package com.libgdx.adventure.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by peter on 11/29/16.
 */

public class DiscRenderer implements ElementRenderer {

    private float radius;
    private Color color;
    private ShapeRenderer shapeRenderer;

    public DiscRenderer(float diameter, Color color, ShapeRenderer shapeRenderer) {
        this.radius = 0.5f * diameter;
        this.color = color;
        this.shapeRenderer = shapeRenderer;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void render(Element element) {
        shapeRenderer.setColor(color);     // use in ShapeType.filled context
        shapeRenderer.circle(element.position.x, element.position.y, element.scale * radius);
    }
}
