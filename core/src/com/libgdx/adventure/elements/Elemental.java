package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by peter on 11/26/16.
 */

public interface Elemental {

    void render();

    // return true if element is selected by given position
    boolean isSelected(Vector2 position);

    // begin-touch action, return true if something changed, call requestRendering, this is safer
    boolean touchBegin(Vector2 position);

    // do drag action, return true if something changed
    boolean touchDrag(Vector2 position,Vector2 deltaPosition);

    // end of touch
    boolean touchEnd();


}
