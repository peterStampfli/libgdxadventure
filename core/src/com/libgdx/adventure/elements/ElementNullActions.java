package com.libgdx.adventure.elements;

/**
 * Created by peter on 11/29/16.
 */

public class ElementNullActions extends ElementActions {

    public ElementNullActions(){
        selector=new ElementNullSelector();
        renderer=new ElementNullRenderer();
        touchBeginner=new ElementNullTouchBeginner();
        touchDragger=new ElementNullTouchDragger();
        touchEnder=new ElementNullTouchEnder();
    }
}
