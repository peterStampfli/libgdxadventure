package com.libgdx.adventure.elements;

/**
 * Created by peter on 11/30/16.
 */

public class ElementNullTouchEnder implements ElementTouchEnder {
    @Override
    public boolean touchEnd(Element element) {
        return false;
    }
}
