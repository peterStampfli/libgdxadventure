package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by peter on 11/30/16.
 */

public class ElementNullTouchBeginner implements ElementTouchBeginner {
    @Override
    public boolean touchBegin(Element element, Vector2 position) {
        return false;
    }
}
