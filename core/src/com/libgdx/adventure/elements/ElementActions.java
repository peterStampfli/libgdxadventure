package com.libgdx.adventure.elements;

/**
 * Created by peter on 11/30/16.
 *
 * A superclass to collect all actions of an element.
 * Baseclass for elements using the actions and for defining sets of actions as prototypes
 */

public class ElementActions {
    public ElementRenderer renderer;
    public ElementSelector selector;
    public ElementTouchBeginner touchBeginner;
    public ElementTouchDragger touchDragger;
    public ElementTouchEnder touchEnder;

    public ElementActions(){};

    public ElementActions(ElementActions elementActions){
        setActions(elementActions);
    }

    public void setActions(ElementActions elementActions){
        selector = elementActions.selector;
        renderer =elementActions.renderer;
        touchBeginner =elementActions.touchBeginner;
        touchDragger =elementActions.touchDragger;
        touchEnder =elementActions.touchEnder;
    }

}
