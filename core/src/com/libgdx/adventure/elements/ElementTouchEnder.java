package com.libgdx.adventure.elements;

/**
 * Created by peter on 11/29/16.
 */

public interface ElementTouchEnder {
    boolean touchEnd(Element element);
}
