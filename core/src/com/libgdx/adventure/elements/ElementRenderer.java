package com.libgdx.adventure.elements;

/**
 * Created by peter on 11/28/16.
 */

public interface ElementRenderer {

    void render(Element element);
}
