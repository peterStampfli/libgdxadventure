package com.libgdx.adventure.elements;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by peter on 11/28/16.
 */

public class Element extends ElementActions implements Elemental {

    public Vector2 position;
    public float angleRad;
    public float scale;


    public Element() {
        position = new Vector2();
        angleRad = 0f;
        scale = 1.0f;
    }

    public Element(ElementActions elementActions) {
        this();
        setActions(elementActions);
    }

    public Element(Element element) {
        this.position = new Vector2(element.position);
        this.angleRad = element.angleRad;
        this.scale = element.scale;
        setActions(element);
    }


    @Override
    public void render() {
        renderer.render(this);
    }

    @Override
    public boolean isSelected(Vector2 position) {
        return selector.isSelected(this, position);
    }

    @Override
    public boolean touchBegin(Vector2 position) {

        return touchBeginner.touchBegin(this, position);
    }

    @Override
    public boolean touchDrag(Vector2 position, Vector2 deltaPosition) {
        return touchDragger.touchDrag(this, position, deltaPosition);
    }

    @Override
    public boolean touchEnd() {
        return touchEnder.touchEnd(this);
    }
}
