package com.libgdx.adventure;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.libgdx.adventure.utilities.Device;
import com.libgdx.adventure.utilities.FollowCamera;
import com.libgdx.adventure.utilities.SimpleLoader;

public class LibGdxAdventure extends Game {

	// general assets _> device
	public Device device;
	public SpriteBatch batch;
	public BitmapFont font;
	public SimpleLoader loader;
	//game specific assets
	public Music rainMusic;

	// screens
	public MainMenueScreen mainMenueScreen;
	public GameScreen gameScreen;

	Viewport viewport;
	FollowCamera followCamera;


	@Override
	public void create () {
		device=new Device().createSpriteBatch().createBitmapFont().createShapeRenderer();
		device.setLogging(true);

		batch=device.spriteBatch;
		font=device.bitmapFont;
		font.getData().setScale(3.0f);

		//camera.setToOrtho(false,800,480);

		loader=device.loader;
		rainMusic=loader.getMusic("rain.mp3");

		rainMusic.setLooping(true);
		rainMusic.play();
		mainMenueScreen=new MainMenueScreen(this);
		gameScreen=new GameScreen(this);

		//setScreen(mainMenueScreen);
		setScreen(new TestScreen(this));

/*
		Elements level1=new Elements("level1");
		Elements level2=new Elements("level2");
		LoggingElement dA=new LoggingElement("dummy A");
		LoggingElement dB=new LoggingElement("dummy B");
		LoggingElement dC=new LoggingElement("dummy C");
		dA.isSelection=false;
		dC.isSelection=true;
		level1.add(dB);
		level1.add(dA);
		level1.add(level2);
		level2.add(dC);

		level1.setReorder(true);
		level1.isSelected(new Vector2());
		level1.renderFrame();
		*/

	}
	@Override
	public void render(){
		//Logger.log(Basic.time);
		super.render();
	}

	@Override
	public void resize(int width,int height){
		super.resize(width, height);
		//camera.setToOrtho(false,width,height);
		//camera.position.x=400;
		//viewport.setScreenX(0);
	}


	@Override
	public void dispose () {

		device.dispose();
	}
}
