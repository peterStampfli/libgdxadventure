package com.libgdx.adventure;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.libgdx.adventure.utilities.*;

import java.util.Iterator;

import static com.badlogic.gdx.math.MathUtils.randomBoolean;

/**
 * Created by peter on 11/8/16.
 */

public class GameScreen extends ScreenAdapter {

    private final LibGdxAdventure game;
    private final Device device;
    private OrthographicCamera camera;
    private FollowCamera followCamera;
    private UserInteraction userInteraction;
    private Viewport viewport;




    private Vector2 bucketPosition;
    private Array<Vector2> raindropPositions;
    private Rectangle bucket;
    private ShapeRenderer shapeRenderer;

    public Texture dropImage;
    public Texture bucketImage;
    public Sound dropSound;


    public GameScreen(LibGdxAdventure libGdxAdventure) {
        game = libGdxAdventure;
        device=game.device;
        camera=new OrthographicCamera(800,480);
        userInteraction=new UserInteraction(camera);

        viewport=new ExtendViewport(800,480,camera);
        followCamera=new com.libgdx.adventure.utilities.FollowCamera(1200,680,camera);
        followCamera.useMouseWheel().setMouseButton(Input.Buttons.RIGHT);
        //followCamera.wasyexKeys();
        followCamera.setRenderAlways(true);

        dropSound= device.loader.getSound("drop.wav");
        dropImage= device.loader.getTexture("droplet.png");
        bucketImage= device.loader.getTexture("bucket.png");
        Basic.linearInterpolation(bucketImage);

        raindropPositions=new Array<Vector2>();
        bucketPosition =new Vector2();
        bucket=new Rectangle();
        bucket.x=400;
        bucket.y=0;
        bucket.width=64;
        bucket.height=64;

        shapeRenderer=device.shapeRenderer;

    }

    @Override
    public void show(){
        Basic.setContinuousRendering(true);
    }

    @Override
    public void resize(int width,int height){
        userInteraction.update(width, height);
        //camera.setToOrtho(false,width,height);
        viewport.update(width, height);
        followCamera.update(width, height);
    }

    private void spawnRaindrop(){
        raindropPositions.add(new Vector2(MathUtils.random(32,800-32),480));
    }

    @Override
    public void render (float dTime) {
        float spawnrate=1;
        if (randomBoolean(spawnrate* dTime)){
            spawnRaindrop();
        }
        Iterator<Vector2> raindrops=raindropPositions.iterator();
        while (raindrops.hasNext()){
            Vector2 raindrop=raindrops.next();
            raindrop.y-=200*dTime;
            if (raindrop.y<0){
                if (Math.abs(raindrop.x-bucketPosition.x)<32){
                    dropSound.play();
                    raindrops.remove();
                }
                else if (raindrop.y+64<0){
                    raindrops.remove();
                }

            }
        }
        float speed=3f;
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            bucketPosition.x += speed;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            bucketPosition.x -= speed;
        }


        bucketPosition.x=MathUtils.clamp(bucketPosition.x,0,800);
            bucketPosition.y=0;
      //      Logger.log("bucket "+bucketPosition.x);

        //bucketPosition.x=0;
        followCamera.follow(bucketPosition);
        viewport.apply();

        Basic.clearBackground(0,0.4f,0.2f);

       //camera.position.x=bucketPosition.x;
        //camera.update();
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(0,0,0.5f,1);
        shapeRenderer.rect(0,0,800,100);

        shapeRenderer.end();

        device.spriteBatch.setProjectionMatrix(camera.combined);

        device.spriteBatch.begin();
        for (Vector2 position:raindropPositions){
            device.spriteBatch.draw(dropImage,position.x-32,position.y);
        }
        device.spriteBatch.draw(bucketImage,bucketPosition.x-32,bucketPosition.y);
        device.spriteBatch.end();

        shapeRenderer.setColor(Color.ORANGE);
        followCamera.renderFrame(shapeRenderer);
    }
}
