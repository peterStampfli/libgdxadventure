package com.libgdx.adventure;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.libgdx.adventure.utilities.Basic;
import com.libgdx.adventure.utilities.Device;

/**
 * Created by peter on 11/7/16.
 */

public class MainMenueScreen extends ScreenAdapter{

    private final LibGdxAdventure game;
    private final Device device;
//  camera things come here
    private OrthographicCamera camera;
    private ShapeRenderer shapeRenderer;

    public MainMenueScreen(LibGdxAdventure libGdxAdventure){
        this.game=libGdxAdventure;
        shapeRenderer=new ShapeRenderer();
        device=game.device;
        camera=new OrthographicCamera();

    }

    Cursor cursor;

    @Override
    public void show(){
        Basic.setContinuousRendering(false);
    }

    @Override
    public void resize(int width,int height){
        camera.setToOrtho(false);

    }

    @Override
    public void render(float delta){
        Basic.clearBackground(Color.CORAL);
        camera.update();
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(0,0,0.5f,1);
        shapeRenderer.rect(0,0,500,500);

        shapeRenderer.end();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.font.draw(game.batch, "Welcome to Drop!!! ", 100, 150);
        game.font.draw(game.batch, "Tap anywhere to beginWithCameraUpdate!", 100, 100);

        game.batch.end();
      //  if(Gdx.input.isTouched()){
            game.setScreen(game.gameScreen);
            dispose();
     //   }
    }

    @Override
    public void dispose(){
    }
}
