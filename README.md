This is my code to make libGdx more readable and easier to use. For more information see my blog https://libgdxadventure.wordpress.com/.

To experiment with the code use the usual libGDX project setup with gradle. The name and game class is "libGdxAdventure" and the package "com.libgdx.adventure".
Then put these files in core->java->"com.libgdx.adventure".


EMail your comments to stampfli@macquality.ch